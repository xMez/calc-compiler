#include <stdio.h>
#include "calc3.h"
#include "y.tab.h"

static int lbl;

int ex(nodeType *p) {
    int lbl1, lbl2;

    if (!p) return 0;
    switch(p->type) {
    case typeCon:       
        printf("\tpushq\t$%d\n", p->con.value); 
        break;
    case typeId:        
        printf("\tpushq\t%c\n", p->id.i + 'a'); 
        break;
    case typeOpr:
        switch(p->opr.oper) {
        case WHILE:
            printf("L%03d:\n", lbl1 = lbl++);
            ex(p->opr.op[0]);       // Condition
            printf("\tL%03d\n", lbl2 = lbl++);  // Append jump destination at the end of the line
            ex(p->opr.op[1]);       // Body
            printf("\tjmp\tL%03d\n", lbl1);
            printf("L%03d:\n", lbl2);
            break;
        case IF:
            ex(p->opr.op[0]);       // Condition
            if (p->opr.nops > 2) {
                /* if else */
                printf("\tL%03d\n", lbl1 = lbl++);  // Append jump destination at the end of the line
                ex(p->opr.op[1]);   // Body
                printf("\tjmp\tL%03d\n", lbl2 = lbl++);
                printf("L%03d:\n", lbl1);
                ex(p->opr.op[2]);   // Else condition
                printf("L%03d:\n", lbl2);
            } else {
                /* if */
                printf("\tL%03d\n", lbl1 = lbl++);  // Append jump destination at the end of the line
                ex(p->opr.op[1]);   // Body
                printf("L%03d:\n", lbl1);
            }
            break;
        case PRINT:     
            ex(p->opr.op[0]);
            printf("\tmovq\t$msg, %%rdi\n");
	        printf("\tpopq\t%%rsi\n");
	        printf("\txorq\t%%rax, %%rax\n");
            printf("\tcall\tprintf\n");    // TODO: Change this to write
            break;
        case '=':       
            ex(p->opr.op[1]);
            printf("\tpopq\t%c\n", p->opr.op[0]->id.i + 'a');
            break;
        case UMINUS:    
            ex(p->opr.op[0]);
            printf("\tpopq\t%%rax\n");
            printf("\tnegq\t%%rax\n");
            printf("\tpushq\t%%rax\n");
            break;
        case FACT:
            ex(p->opr.op[0]);
            printf("\tpopq\t%%rdi\n");
            printf("\tcall\tfact\n");   // Tested and done
            printf("\tpushq\t%%rax\n");
            break;
        case LNTWO:
            ex(p->opr.op[0]);
            printf("\tpopq\t%%rdi\n");
            printf("\tcall\tlntwo\n");  // Tested and done
            printf("\tpushq\t%%rax\n");
            break;
        default:
            ex(p->opr.op[0]);
            ex(p->opr.op[1]);           // These have to be poped in the opposite order
            switch(p->opr.oper) {       // Always store the result in %rax
	        case GCD:                   // Function uses different registers
                printf("\tpopq\t%%rsi\n");
                printf("\tpopq\t%%rdi\n");
                printf("\tcall\tgcd\n");// Tested and done
                printf("\tpushq\t%%rax\n");
                break;
            case '+':
                printf("\tpopq\t%%rbx\n");
                printf("\tpopq\t%%rax\n");
                printf("\taddq\t%%rbx, %%rax\n");
                printf("\tpushq\t%%rax\n");
                break;
            case '-':
                printf("\tpopq\t%%rbx\n");
                printf("\tpopq\t%%rax\n");
                printf("\tsubq\t%%rbx, %%rax\n");
                printf("\tpushq\t%%rax\n");
                break; 
            case '*':
                printf("\tpopq\t%%rbx\n");
                printf("\tpopq\t%%rax\n");
                printf("\timulq\t%%rbx, %%rax\n");
                printf("\tpushq\t%%rax\n");
                break;
            case '/':
                printf("\tpopq\t%%rbx\n");
                printf("\tpopq\t%%rax\n");
                printf("\tcqto\n");
                printf("\tidivq\t%%rbx\n");
                printf("\tpushq\t%%rax\n");
                break;
            case '<': // These have to be opposite because of how the rest of the setup is      
                printf("\tpopq\t%%rbx\n");
                printf("\tpopq\t%%rax\n");
                printf("\tcmpq\t%%rbx, %%rax\n");
                printf("\tjge");
                break;
            case '>':
                printf("\tpopq\t%%rbx\n");
                printf("\tpopq\t%%rax\n");
                printf("\tcmpq\t%%rbx, %%rax\n");
                printf("\tjle");
                break;
            case GE:
                printf("\tpopq\t%%rbx\n");
                printf("\tpopq\t%%rax\n");
                printf("\tcmpq\t%%rbx, %%rax\n");
                printf("\tjl"); 
                break;
            case LE:
                printf("\tpopq\t%%rbx\n");
                printf("\tpopq\t%%rax\n");
                printf("\tcmpq\t%%rbx, %%rax\n");
                printf("\tjg"); 
                break;
            case NE:
                printf("\tpopq\t%%rbx\n");
                printf("\tpopq\t%%rax\n");
                printf("\tcmpq\t%%rbx, %%rax\n");
                printf("\tje"); 
                break;
            case EQ:
                printf("\tpopq\t%%rbx\n");
                printf("\tpopq\t%%rax\n");
                printf("\tcmpq\t%%rbx, %%rax\n");
                printf("\tjne");
                break;
            }
        }
    }
    return 0;
}
