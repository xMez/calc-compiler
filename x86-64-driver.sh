#!/bin/bash
# Read the first argument
# Check if it has .calc file extention
# Write the prologe to a file with the same name as the input file but with .s ending
# Compile the input file and append the output the the .s file with the prologue
# Append epilogue for calling exit syscall
# Call gcc to link and assemble all needed files output to a new file with the same filename but no file extension

CC="gcc"
CFLAGS="-m64 -g -static"
LIB="lib/math.lib"


prologue() # cat <<EOF lets us output multiple lines easily
{
    cat <<EOF
.data
	msg:	.asciz	"%d\n"
	a:		.quad	0
	b:		.quad	0
	c:		.quad	0
	d:		.quad	0
	e:		.quad	0
	f:		.quad	0
	g:		.quad	0
	h:		.quad	0
	i:		.quad	0
	j:		.quad	0
	k:		.quad	0
	l:		.quad	0
	m:		.quad	0
	n:		.quad	0
	o:		.quad	0
	p:		.quad	0
	q:		.quad	0
	r:		.quad	0
	s:		.quad	0
	t:		.quad	0
	u:		.quad	0
	v:		.quad	0
	w:		.quad	0
	x:		.quad	0
	y:		.quad	0
	z:		.quad	0
.text
.globl main
.type   main, @function

main:
EOF
}

compiler()
{
	cat $1 | ./bin/calc3i.exe
}

epilogue()
{
    # The epilogue also needs to contain the write function
    # that uses syscall. The limitation of the syscall is that we cannot
    # print integers directly so we need to make it into a string.
    cat <<EOF
    jmp exit

exit:
    mov     \$60, %rax   /* syscall number */
	mov     \$0, %rdi    /* exit status */
	syscall
EOF
}

if [ $# -lt 1 ]; then
	echo "$0 HELP TEXT HERE"
	echo
	exit 1
fi

FILE=$1

# if ! [[ -f "$FILE" ]]
# then
#     echo "Argument is not a file"
#     exit 1
# fi

if [[ ${FILE: -5} != ".calc" ]]
then
    echo "Expected a .calc file"
    exit 1
fi

NAME=$(echo "$FILE" | rev | cut -c 6- | rev)

prologue > "$NAME.s"
compiler $FILE >> "$NAME.s"
epilogue >> "$NAME.s"

gcc $CFLAGS $NAME.s $LIB -o $NAME