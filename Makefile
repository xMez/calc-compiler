AS = as
AR = ar
CC = gcc

LEX_DIR = lexyacc-code
LIB_DIR = lib
SRC_DIR = src
OBJ_DIR = obj
BIN_DIR = bin

SRCS = $(LEX_DIR)/calc3i.c
OBJS = $(OBJ_DIR)/y.tab.o $(OBJ_DIR)/lex.yy.o
lib_SRCS = $(wildcard $(SRC_DIR)/*.S)
lib_OBJS = $(patsubst $(SRC_DIR)/%.S,$(OBJ_DIR)/%.o,$(lib_SRCS))
PROG = calc3i.exe
LIBS = math.lib

all: $(PROG) $(LIBS)

$(LEX_DIR)/y.tab.c: $(LEX_DIR)/calc3.y
	bison -y -d $^ -o $@

$(LEX_DIR)/lex.yy.c: $(LEX_DIR)/calc3.l
	flex -o $@ $^

$(OBJ_DIR)/%.o: $(LEX_DIR)/%.c
	$(CC) -c $^ -o $@

$(PROG): $(OBJS) $(SRCS)
	$(CC) $^ -o $(BIN_DIR)/$@

$(OBJ_DIR)/%.o: $(SRC_DIR)/%.S
	$(AS) -o $@ --64 $^

$(LIBS): $(lib_OBJS)
	$(AR) rcs $(LIB_DIR)/$@ $^

clean:
	rm -f $(OBJ_DIR)/*.o $(BIN_DIR)/$(PROG) $(LIB_DIR)/$(LIBS) $(LEX_DIR)/y.tab.c $(LEX_DIR)/y.tab.h $(LEX_DIR)/lex.yy.c